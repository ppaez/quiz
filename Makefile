test:
	python3-coverage run -m unittest discover -s tests -t . && python3-coverage report -m *.py

.PHONY: test
