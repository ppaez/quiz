------------
Introduction
------------

Quiz is a library that allows an application to use learning material
in an interactive way.

The learning material can be easily created using a text editor.

A unit of learning material, known as a quiz, covers a specific topic
and provides a few questions to be answered.  Incorrect answers or
options provide some feedback and ask for a retry of the same
question.  Once the correct answer or options are selected, the next
question is offered, and so on.


Use
===

Load a test file and get an instance of :class:`~quiz.Quiz`::

  import quiz

  quiz = quiz.load('path-to-test-file')

To use the content::

  for question in quiz.questions:
      print(question.text)
      for option in question.options:
          print(option.text)
      indexes = input('Enter one or more numbers: ')
      question.evaluate(indexes)

To access a dictionary that provides `title`, `description`,
`estimated completion time`, `begin` and `end` text::

  info = quiz.get_info('path-to-test-file')
  print(info['title'])
  print(info['description'])
  print('Estimated completion time:', info['time'])
  print(info['begin'])
  .
  .
  .
  print(info['end'])

Model
=====

A test is made of one or more questions, each question may have
one or more options as possible answers, and either a single
option is the correct answer, or two or more options are the
correct answer.

Format of the content file
==========================

There is an information section at the beginning of the file::

 title: 
 description:
 time:     
 begin:
 end:


Three prefixes are defined:

- ``q:`` *text of the question*
- ``o:`` *text of an incorrect option* ``|`` *text of the feedback*
- ``a:`` *text of a correct option* ``|`` *text of the feedback*

