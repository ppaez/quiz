import unittest


class GetInfo(unittest.TestCase):

    def setUp(self):
        from unittest.mock import Mock
        import quiz

        self.afile = Mock()
        self.afile.read.return_value = ''
        quiz.open = Mock(return_value=self.afile)

    def tearDown(self):
        import quiz

        del quiz.open

    def test_info(self):
        from quiz import get_info

        text = 'title: title\n' \
            'description: descr\n' \
            'time: 10m\n' \
            'begin: Welcome\n' \
            'end: Finished\n'
        self.afile.read.return_value = text

        result = get_info('path')
        expected = {'title': 'title',
                    'description': 'descr',
                    'time': '10m',
                    'begin': 'Welcome',
                    'end': 'Finished'}

        self.assertEqual(expected, result)


class Load(unittest.TestCase):

    def setUp(self):
        from unittest.mock import Mock
        import quiz

        self.afile = Mock()
        self.afile.read.return_value = ''
        quiz.open = Mock(return_value=self.afile)

    def tearDown(self):
        import quiz

        del quiz.open

    def test_answer(self):
        from quiz import load

        text = '''q: qtext\na: atext\n'''
        self.afile.read.return_value = text

        t = load('path')
        self.assertEqual(1, len(t.questions))

    def test_answers(self):
        from quiz import load

        text = '''q: qtext\na: atext1\na: atext2\n'''
        self.afile.read.return_value = text

        t = load('path')
        self.assertEqual(1, len(t.questions))

    def test_option(self):
        from quiz import load

        text = '''q: qtext\no: text\n'''
        self.afile.read.return_value = text

        t = load('path')
        self.assertEqual(1, len(t.questions))

    def test_options(self):
        from quiz import load

        text = '''q: qtext\no: otext1\no: otext2\n'''
        self.afile.read.return_value = text

        t = load('path')
        self.assertEqual(1, len(t.questions))
        self.assertEqual(2, len(t.questions[0].options))

    def test_questions(self):
        from quiz import load

        text = '''q: qtext1\nq: qtext2\n'''
        self.afile.read.return_value = text

        t = load('path')
        self.assertEqual(2, len(t.questions))

class Quiz(unittest.TestCase):

    def test_instance(self):
        from quiz import Quiz

        t = Quiz()
        self.assertEqual([], t.questions)

    def test_instance(self):
        from quiz import Quiz

        t = Quiz()
        self.assertEqual([], t.questions)

    def test_question(self):
        from quiz import Quiz
        from quiz import Question
        from quiz import Option

        a = Option('answer')
        o = Option('text')
        q = Question('text', [a], [o])

        t = Quiz(questions=[q])
        self.assertEqual([q], t.questions)


class Question(unittest.TestCase):

    def test_instance(self):
        from quiz import Question

        q = Question()
        self.assertEqual([], q.answers)

    def test_text(self):
        from quiz import Question

        q = Question('text', [], [])
        self.assertEqual('text', q.text)

    def test_answer(self):
        from quiz import Question
        from quiz import Option

        a = Option('answer')
        q = Question('text', [a], [])
        self.assertEqual([a], q.answers)

    def test_option(self):
        from quiz import Question
        from quiz import Option

        o = Option('text')
        q = Question('text', [], [o])
        self.assertEqual([o], q.options)

    def test_option_index(self):
        from quiz import Question
        from quiz import Option

        o = Option('text')
        q = Question('text', [], [o])
        self.assertEqual(1, q.options[0].index)

    def test_answer_option(self):
        from quiz import Question
        from quiz import Option

        a = Option('answer')
        o = Option('text')
        q = Question('text', [a], [o])
        self.assertEqual([a, o], q.options)


class Evaluate(unittest.TestCase):

    def test_one_answer_one_option_pass(self):
        from quiz import Question
        from quiz import Option

        a = Option('answer')
        o = Option('text')
        q = Question('text', [a], [o])
        self.assertEqual(True, q.evaluate([1]))
        self.assertEqual(1, len(q.correct_options))
        self.assertEqual(0, len(q.surplus_options))
        self.assertEqual(0, len(q.missing_options))

    def test_one_answer_one_option_fail(self):
        from quiz import Question
        from quiz import Option

        a = Option('answer')
        o = Option('text')
        q = Question('text', [a], [o])
        self.assertEqual(False, q.evaluate([2]))
        self.assertEqual(0, len(q.correct_options))
        self.assertEqual(1, len(q.surplus_options))
        self.assertEqual(1, len(q.missing_options))

    def test_set_passed(self):
        from quiz import Question
        from quiz import Option

        a = Option('answer')
        o = Option('text')
        q = Question('text', [a], [o])
        q.evaluate([1])
        self.assertTrue(q.passed)

    def test_two_answers_one_option_pass(self):
        from quiz import Question
        from quiz import Option

        a1 = Option('answer1')
        a2 = Option('answer2')
        o = Option('text')
        q = Question('text', [a1, a2], [o])
        self.assertEqual(True, q.evaluate([1, 2]))
        self.assertEqual(2, len(q.correct_options))
        self.assertEqual(0, len(q.surplus_options))
        self.assertEqual(0, len(q.missing_options))

    def test_two_answers_one_option_fail_missing(self):
        from quiz import Question
        from quiz import Option

        a1 = Option('answer1')
        a2 = Option('answer2')
        o = Option('text')
        q = Question('text', [a1, a2], [o])
        self.assertEqual(False, q.evaluate([1]))
        self.assertEqual(1, len(q.correct_options))
        self.assertEqual(0, len(q.surplus_options))
        self.assertEqual(1, len(q.missing_options))

    def test_two_answers_one_option_fail_surplus(self):
        from quiz import Question
        from quiz import Option

        a1 = Option('answer1')
        a2 = Option('answer2')
        o = Option('text')
        q = Question('text', [a1, a2], [o])
        self.assertEqual(False, q.evaluate([1, 2, 3]))
        self.assertEqual(2, len(q.correct_options))
        self.assertEqual(1, len(q.surplus_options))
        self.assertEqual(0, len(q.missing_options))


class Feedbacks(unittest.TestCase):

    def test_correct_options(self):
        from quiz import Question
        from quiz import Option

        a = Option('answer', 'feedback1')
        o = Option('text', 'feedback2')
        q = Question('text', [a], [o])
        q.evaluate([1])
        self.assertEqual(['feedback1'], q.feedbacks())

    def test_missing_options(self):
        from quiz import Question
        from quiz import Option

        a = Option('answer', 'feedback1')
        o = Option('text', 'feedback2')
        q = Question('text', [a], [o])
        q.evaluate([])
        self.assertEqual([q.incomplete_feedback], q.feedbacks())

    def test_surplus_options(self):
        from quiz import Question
        from quiz import Option

        a = Option('answer', 'feedback1')
        o = Option('text', 'feedback2')
        q = Question('text', [a], [o])
        q.evaluate([2])
        self.assertEqual(['feedback2'], q.feedbacks())


class Option(unittest.TestCase):

    def test_instance(self):
        from quiz import Option

        o = Option()
        self.assertEqual('', o.text)

    def test_data(self):
        from quiz import Option

        o = Option('text', 'feedback')
        self.assertEqual('text', o.text)
        self.assertEqual('feedback', o.feedback)
