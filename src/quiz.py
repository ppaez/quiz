import re


def get_info(quiz_file):
    '''Read quiz_file and return a dictionary with information'''

    info = {}
    t = open(quiz_file).read()
    for field in ('title', 'description', 'time', 'begin', 'end'):
        pattern = f'^{field}: *(.*) *'
        m = re.search(pattern, t, re.I+re.M)
        if m:
            info[field] = m.group(1)
        else:
            info[field] = ''

    return info


def load(quiz_file):
    'Read quiz_file and return a Quiz instance'

    questions = []
    answers = []
    options = []
    in_question = False
    t = open(quiz_file).read()

    offset = 0
    reo = re.compile('^([qao]):', flags=re.M+re.I+re.S)
    m = reo.search(t, offset)
    while m:
        code = m.group(1)
        offset = m.end()
        m = reo.search(t, offset)
        if m:
            value = t[offset:m.start()]
            parts = value.split('|')
            text = parts[0].strip()
            feedback = parts[1].strip() if len(parts) > 1 else ''
            if code == 'a':
                answers.append((text, feedback))
            elif code == 'o':
                options.append((text, feedback))
        if code == 'q':
            if in_question:
                answer_list = [Option(text, feedback) for
                                   text, feedback in answers]
                option_list = [Option(text, feedback) for
                                   text, feedback in options]
                q = Question(qtext, answer_list, option_list)
                questions.append(q)
                answers = []
                options = []
            qtext = text
            in_question = True

    value = t[offset:]
    parts = value.split('|')
    text = parts[0].strip()
    feedback = parts[1].strip() if len(parts) > 1 else ''
    if code == 'q':
        qtext = text
    elif code == 'a':
        answers.append((text, feedback))
    elif code == 'o':
        options.append((text, feedback))
    answer_list = [Option(text, feedback) for
                       text, feedback in answers]
    option_list = [Option(text, feedback) for
                       text, feedback in options]
    q = Question(qtext, answer_list, option_list)
    questions.append(q)

    return Quiz(questions)


class Quiz:
    """Information about a test

    It may have zero or more questions
    """

    def __init__(self, questions=None):
        if questions is None:
            questions = []
        self.questions = questions


class Question:
    """Information about a question"""

    def __init__(self, text='', answers=None, options=None):
        self.text = text
        if answers is None:
            answers = []
        self.answers = answers
        if options is None:
            options = []
        self.options = answers + options
        for i, o in enumerate(self.options):
            o.index = i + 1
        self.passed = False
        self.incomplete_feedback = 'Incomplete answer'

    def evaluate(self, indexes):
        """Return True if all indexes correspond to answers"""

        self.correct_options = []
        self.surplus_options = []
        self.missing_options = self.answers[:]

        for index in indexes:
            option = self.options[int(index)-1]
            if option in self.missing_options:
                self.missing_options.remove(option)
                self.correct_options.append(option)
            else:
                self.surplus_options.append(option)
                return False
        if self.missing_options:
            return False
        self.passed = True
        return True

    def feedbacks(self):
        """Return the feedbacks as per correct, missing and surplus options"""

        if self.surplus_options:
            return [o.feedback for o in self.surplus_options]
        elif self.missing_options:
            return [self.incomplete_feedback]
        return [o.feedback for o in self.correct_options]


class Option:
    """Information about an option"""

    def __init__(self, text='', feedback=''):
        self.text = text
        self.feedback = feedback
        self.index = 0
